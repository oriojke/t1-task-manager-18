package ru.t1.didyk.taskmanager.command.user;

import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "Update user profile.";
    }

    @Override
    public String getName() {
        return "update-user-profile";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserID();
        System.out.println("[USER PROFILE UPDATE]");
        System.out.println("FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }
}
