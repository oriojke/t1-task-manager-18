package ru.t1.didyk.taskmanager.command.system;

import ru.t1.didyk.taskmanager.api.service.ICommandService;
import ru.t1.didyk.taskmanager.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
