package ru.t1.didyk.taskmanager.api.controller;

public interface ICommandController {

    void showVersion();

    void showHelp();

    void showSystemInfo();

    void showErrorArgument();

    void showErrorCommand();

    void showAbout();

    void showExit();

}
