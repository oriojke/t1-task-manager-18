package ru.t1.didyk.taskmanager.api.service;

import ru.t1.didyk.taskmanager.model.User;

public interface IAuthService {

    User registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserID();

    User getUser();

}
